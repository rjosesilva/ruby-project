# Language: Ruby, Level: Level 3
Quando("cadastrar um novo supplier") do

  @home_admin = HomeAdmin.new
  @supplier_management = SupplierManagement.new
  @add_supplier = AddSupplier.new

  @home_admin.wait_for_title_home_admin

  #clica no menu´s 'ACCOUNTS' e em seguida 'SUPPLIERS' na página 'Home'
  @home_admin.registerNewSupplier

  #clica no botão 'Add' na página 'Suppliers Management'
  @supplier_management.clickAddSuppliers

  #adiciona um novo supllier na página 'Add Supplier'
  @name = "#{Faker::Name.first_name}"

  #cria o object supplier
  @new_supplier = OpenStruct.new
  @new_supplier.first_name    = @name
  @new_supplier.last_name     = Faker::Name.last_name
  @new_supplier.email = Faker::Internet.free_email(@name)
  @new_supplier.password = Faker::Lorem.characters(8)
  @new_supplier.phone = Faker::PhoneNumber.cell_phone


  @add_supplier.AddNewSupplier(@new_supplier)

  @supplier_management. wait_for_title_page

  #pesquisa um supplier na página 'Suppliers Management'
  @supplier_management.searchSupplier(@new_supplier.email)

end

Entao("devo visualizar o novo supplier cadastrado") do

  #realiza os asserts
  expect(@supplier_management.lista_suppliers.size).to eql 1
  expect(@supplier_management.lista_suppliers.first).to have_content @new_supplier.first_name
  expect(@supplier_management.lista_suppliers.first).to have_content @new_supplier.last_name
  expect(@supplier_management.lista_suppliers.first).to have_content @new_supplier.email

end
