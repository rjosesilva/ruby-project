# Language: Ruby, Level: Level 3
Dado("que eu estiver logado no phptravels com as credenciais") do |table|
  @login = LoginPanel.new
  @login.load
  @login.wait_for_title_login_panel
  email = table.rows_hash['email']
  senha = table.rows_hash['senha']
  @login.realizar_login(email,senha)

end

Dado("que eu estiver logado no phptravels com as credenciais {string} e {string}") do |email, senha|
    @login.realizar_login(email,senha)
end

Entao("devo ser redirecionado para home e visualizar a seguinte {string}") do |string|
  pending # Write code here that turns the phrase above into concrete actions
end
