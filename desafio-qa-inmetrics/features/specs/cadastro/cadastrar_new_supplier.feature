# language: pt

Funcionalidade: cadastrar novos suppliers

#eu como usuário do phptravels quero cadastrar um novo supplier
@regressivo
Cenario: cadastrar um novo supplier com sucesso
Dado que eu estiver logado no phptravels com as credenciais
|email               |admin@phptravels.com    |
|senha               |demoadmin               |
Quando cadastrar um novo supplier
Entao devo visualizar o novo supplier cadastrado
