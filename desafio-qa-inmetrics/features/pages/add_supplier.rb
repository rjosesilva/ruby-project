# Language: Ruby, Level: Level 3

class AddSupplier < SitePrism::Page


#variavel
attr_accessor :email_supplier

#campos do formulário de cadastro de supplier

#titulos da página
element :title_add_supplier, :xpath, '//div[contains(text(),"Add Supplier")]'

#botoes
element :button_submit, :xpath, '//div[contains(text(),"Add Supplier")]/following::button[1]'

#campos inputs
element :input_first_name, 'input[name="fname"]'
element :input_last_name, 'input[name="lname"]'
element :input_email, 'input[name="email"]'
element :input_password, 'input[name="password"]'
element :input_mobile_number, 'input[name="mobile"]'
element :input_address_one, 'input[name="address1"]'
element :input_address_two, 'input[name="address2"]'
element :input_name, 'input[name="itemname"]'
element :input_assign_hotels, '#s2id_autogen4'
element :input_assign_tours, '#s2id_autogen6'
element :input_assign_cars, '#s2id_autogen8'

#selects
element :select_country, 'select[name="country"]'
element :select_status, 'select[name="status"]'
element :select_supplier_for, 'select[name="applyfor"]'

#checkbox´s
element :checkbox_email_newsletter_subscriber, :xpath, '//input[@name="itemname"]/following::label[1]'

#section ADD
element :checkbox_add_hotels,:xpath, '//label[contains(text(),"Assign Cars")]/following::div[contains(text(),"Add")]/following::label[1]'

#section EDIT
element :checkbox_edi_hotels, :xpath, '//label[contains(text(),"Assign Cars")]/following::div[contains(text(),"Add")]/following::label[6]'

#section REMOVE
element :checkbox_remove_hotels, :xpath, '//label[contains(text(),"Assign Cars")]/following::div[contains(text(),"Add")]/following::label[11]'


#outros elementos
element :items_hotels_tours_cars, '.select2-result-label'

#cadastra um novo supplier na página 'ADD SUPPLIER'
def AddNewSupplier(new_supplier)

  #self.emailSupplier = Faker::Internet.email
  self.email_supplier = "silvarj@gmail.com"

  input_first_name.set(new_supplier.first_name)
  input_last_name.set (new_supplier.last_name)
  input_email.set(new_supplier.email)
  input_password.set(new_supplier.password)
  input_mobile_number.set(new_supplier.phone)
  select_country.select("Brazil")
  input_address_one.set "rua teste 1"
  input_address_two.set "rua teste 2"
  select_status.select("Enabled")
  select_supplier_for.select("Hotels")
  input_name.set "ronaldo testando"
  checkbox_email_newsletter_subscriber.click
  input_assign_hotels.set("Rendezvous Hotels")
  find('#s2id_autogen4  ').native.send_keys(:enter)
  find('#s2id_autogen4  ').native.send_keys(:tab)
  #selectAssignTours.click
  input_assign_tours.set("6 Days Around Thailand")
  find('#s2id_autogen6  ').native.send_keys(:enter)
  find('#s2id_autogen6  ').native.send_keys(:tab)
  #selectAssignCars.click
  input_assign_cars.set("Ford Focus 2014")
  find('#s2id_autogen8  ').native.send_keys(:enter)
  find('#s2id_autogen8  ').native.send_keys(:tab)

  checkbox_add_hotels.click
  checkbox_edi_hotels.click
  checkbox_remove_hotels.click
  button_submit.click

end

end
