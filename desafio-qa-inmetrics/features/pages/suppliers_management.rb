# Language: Ruby, Level: Level 3
class SupplierManagement < SitePrism::Page

  #variavel
  attr_accessor :email

  #titulos
  element :title_page, :xpath, '//div[contains(text(),"Suppliers Management")]'

  #botoes
  element :button_add, :xpath, '//*[@id="content"]/div/form/button'
  element :button_search, :xpath, '//a[contains(text(),"Search")]'
  element :button_go, :xpath, '//a[contains(text(),"Go")]'

  #campos input´s
  element :input_search_phrase, 'input[name="phrase"]'

  #tabela de elementos
  elements :lista_suppliers, :xpath, '//button[contains(text(),"Add")]/following::table//tbody//tr'

  #clica apenas no botão '+ADD' na página 'SUPPLIERS MANAGEMENT'
  def clickAddSuppliers
    button_add.click
  end

  #pesquisa um supplier
  def searchSupplier(emailSupplier)
    button_search.click
    input_search_phrase.set(emailSupplier)
    button_go.click
  end

end
