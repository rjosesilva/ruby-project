# Language: Ruby, Level: Level 3
class HomeAdmin < SitePrism::Page

  #titulos da página
  element :title_home_admin, :xpath, '//strong[contains(text(),"Dashboard")]'

  #links da página
  element :link_accounts, 'a[href="#ACCOUNTS"]'
  element :link_suppliers, :xpath, '//a[@href="#ACCOUNTS"]/following::li[2]'

  #clica no menu 'ACCOUNTS' e depois clica no menu 'SUPPLIER'
  def registerNewSupplier
    link_accounts.click
    link_suppliers.click
  end

end
