# Language: Ruby, Level: Level 3
class LoginPanel < SitePrism::Page

#url da página principal
set_url 'https://www.phptravels.net/admin'

#titulo da página
element :title_login_panel, :xpath,'//h2[contains(text(),"Login Panel")]'

#botoes
element :button_login, 'button[type="submit"]'

#campos input´s
element :input_email, 'input[name="email"]'
element :input_password, 'input[name="password"]'

#checkbox´s
element :checkbox_remember_me, 'input[name="remember"]'

#links da página
element :link_forget_password, 'a[id="link-forgot"]'


#realiza o login na página "Login Panel"
def realizar_login(email,senha)

  input_email.set(email)
  input_password.set(senha)
  button_login.click

end


def cadastrarNovoSupplier

end

end
